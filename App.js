import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Login from './src/screens/login/Login';
import Home from './src/screens/home/Home';
import Splash from './src/screens/splash/Splash';

const Routes = createStackNavigator({
  Splash: {
    screen: Splash,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      title: 'Home'
    }
  }
}, {
  initialRouteName: 'Splash'
})

export default createAppContainer(Routes);