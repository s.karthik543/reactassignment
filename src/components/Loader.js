import React from 'react';
import { Modal, View, ActivityIndicator, Text,Platform, StyleSheet } from 'react-native';

export default ({ visible }) => {

    return (
        <Modal transparent={true} animationType='none' visible={visible} onRequestClose={() => { }}>

            <View style={styles.dialogContainer}  >
                <View style={{ height: 150, width: 150, backgroundColor: 'white', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size={Platform.OS === 'android' ? 50 : 'large'} color="#E64A19" />

                    <Text style={{ fontSize: 16, color: 'black', marginTop: 5 }} >
                        {'Please wait..!'}
                    </Text>
                </View>
            </View>

        </Modal>
    )
}

const styles = StyleSheet.create({
    dialogContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    }
})