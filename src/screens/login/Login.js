import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    AsyncStorage,
    SafeAreaView,
} from 'react-native';
import {
    GoogleSigninButton,
    GoogleSignin
} from 'react-native-google-signin';
import {
    StackActions,
    NavigationActions
} from 'react-navigation';
import { StorageKeys } from '../../utils/Constants';

class Login extends Component {

    componentDidMount() {
        GoogleSignin.configure({
            webClientId: '853422068941-ei8je3sglvjn5ikoobcqoefvkj2bhqcr.apps.googleusercontent.com',
            offlineAccess: true,
        });
    }

    handleGoogleSign = () => {
        GoogleSignin.hasPlayServices()
            .then((result) => {
                if (result) {
                    GoogleSignin.signIn()
                        .then((userInfo) => {
                            AsyncStorage.setItem(StorageKeys.IS_LOGGED_IN, 'success')
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'Home' })],
                            });
                            this.props.navigation.dispatch(resetAction);
                        })
                }
            }).catch((error) => {
                if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    Alert.alert('cancelled');
                } else if (error.code === statusCodes.IN_PROGRESS) {
                    Alert.alert('in progress');
                } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                    Alert.alert('play services not available or outdated');
                } else {
                    Alert.alert('Something went wrong', error.toString());
                }
            });
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.appName}>
                    ASSIGNMENT
                </Text>
                <GoogleSigninButton
                    style={{ width: 192, height: 48 }}
                    size={GoogleSigninButton.Size.Wide}
                    onPress={this.handleGoogleSign}
                    color={GoogleSigninButton.Color.Dark} />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    appName: {
        fontSize: 25,
        fontWeight: '600',
        color: '#000000',
        marginBottom: 100,
        marginTop: 150
    }
})

export default Login;