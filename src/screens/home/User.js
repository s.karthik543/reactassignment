import React, { PureComponent } from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet
} from 'react-native';

export default class User extends PureComponent {

    render() {
        const { name, username, email, address } = this.props.data;

        return (
            <View style={styles.itemContainer}>

                <Text style={styles.headerLabel}>
                    Name : <Text style={styles.valueLabel}>
                        {name}
                    </Text>
                </Text>

                <Text style={styles.headerLabel}>
                    Username : <Text style={styles.valueLabel}>
                        {username}
                    </Text>
                </Text>

                <Text style={styles.headerLabel}>
                    Email : <Text style={styles.valueLabel}>
                        {email}
                    </Text>
                </Text>

                <Text style={styles.headerLabel}>
                    Address : <Text style={styles.valueLabel}>
                        {`${address.street}, ${address.suite}, ${address.city} ZipCode: ${address.suite}`}
                    </Text>
                </Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        marginHorizontal: 16,
        marginVertical: 5,
        paddingVertical: 10,
        paddingHorizontal: 16,
        borderRadius: 5,
        ...Platform.select({
            android: {
                elevation: 2
            }
        }),
        backgroundColor: '#FFFFFF'
    },
    headerLabel: {
        fontSize: 14,
        fontWeight: '700',
        color: '#000000',
        flex: 1,
        lineHeight: 20
    },
    valueLabel: {
        fontWeight: '600'
    }
})