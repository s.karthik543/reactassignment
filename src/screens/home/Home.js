import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    SafeAreaView
} from 'react-native';
import Loader from '../../components/Loader';
import User from './User';

class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            users: []
        }
    } 

    componentDidMount() {
        this.fetchUsers();
    }

    fetchUsers = () => {

        this.setState({ isLoading: true })

        fetch('https://jsonplaceholder.typicode.com/users')
            .then((result) => result.json())
            .then((respone) => {
                this.setState({ isLoading: false, users: respone })
            }).catch((error) => {
                this.setState({ isLoading: false })
            })
    }

    render() {
        const { isLoading, users } = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <Loader visible={isLoading} />
                <FlatList
                    style={{ flex: 1 }}
                    data={users}
                    keyExtractor={(item) => `${item.id}`}
                    renderItem={({ item }) => <User data={item} />} />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

export default Home;