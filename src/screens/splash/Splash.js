import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    AsyncStorage,
    SafeAreaView
} from 'react-native';
import {
    StackActions,
    NavigationActions
} from 'react-navigation';
import { StorageKeys } from '../../utils/Constants';

class Splash extends Component {

    componentDidMount() {
        setTimeout(() => {
            AsyncStorage.getItem(StorageKeys.IS_LOGGED_IN)
                .then((result) => {
                    let route = 'Login'
                    if (result === 'success') {
                        route = 'Home'
                    }
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: route })],
                    });
                    this.props.navigation.dispatch(resetAction);
                })
        }, 2000)
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.appName}>
                    ASSIGNMENT
                </Text>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center'
    },
    appName: {
        fontSize: 25,
        fontWeight: '600',
        color: '#000000'
    }
});

export default Splash;